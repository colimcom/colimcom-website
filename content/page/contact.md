---
title: "Contacto"
subtitle: "Sientete libre de contactar con nosotros si tienes alguna duda"
comments: false
---

{{< rawhtml >}}
<form method="post" action="https://forms.un-static.com/forms/e88edccd754e7e123dcae4aa87c93c2ad49d3e80">
  <div class="form-group row">
    <label for="name" class="col-4 col-form-label">Nombre</label>
    <div class="col-8">
      <div class="input-group">
        <div class="input-group-addon">
          <i class="fa fa-user"></i>
        </div>
        <input id="name" name="name" placeholder="Introduzca su nombre" type="text" required="required" class="form-control">
      </div>
    </div>
  </div>
  <div class="form-group row">
    <label for="email" class="col-4 col-form-label">E-mail</label>
    <div class="col-8">
      <div class="input-group">
        <div class="input-group-addon">
          <i class="fa fa-envelope"></i>
        </div>
        <input id="email" name="email" placeholder="Ingrese su dirección de correo electrónico" type="text" required="required" class="form-control">
      </div>
    </div>
  </div>
  <div class="form-group row">
    <label for="message" class="col-4 col-form-label">Mensaje</label>
    <div class="col-8">
      <textarea id="message" name="message" cols="40" rows="10" required="required" class="form-control"></textarea>
    </div>
  </div>
  <div class="form-group row">
    <div class="offset-4 col-8">
      <button name="submit" type="submit" class="btn btn-primary">Enviar</button>
    </div>
  </div>
</form>
{{< /rawhtml >}}
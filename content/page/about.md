---
title: Acerca de Nosotros
subtitle: Te invitamos a conocernos un poco
comments: false
---

{{< rawhtml >}}
<div style="text-align:center">
<table style="border:none;">
		<tr>
			<td style="border:none; text-align:justify">Colombiana de Importaciones y Comercialización S.A.S. nace con el objeto de brindar soluciones tecnológicas e industriales a las compañias Colombianas.<br/>&nbsp;</td>
		</tr>
</table>
</div>
{{< /rawhtml >}}

### Quienes Somos

{{< rawhtml >}}
<div style="text-align:center">
<table style="border:none;">
		<tr>
			<td style="border:none; text-align:justify">Somos una empresa Colombiana enfocada en la importación y comercialización de todo tipo de solución que su empresa necesite. Igualmente brindamos el servicio de consultorias profesionales en todas las áreas técnicas con el fin de asesorarlo y ayudarlo a tomar las mejores decisiones para su negocio.<br/>&nbsp;</td>
		</tr>
</table>
</div>
{{< /rawhtml >}}


### Misión

{{< rawhtml >}}
<div style="text-align:center">
<table style="border:none;">
		<tr>
			<td style="border:none; text-align:justify">COLIMCOM S.A.S., es una organización innovadora dedicada a la importación y comercialización de productos, comprometida con la satisfacción de nuestros clientes, disponemos de una amplia gama de productos y servicios de calidad a precios competitivos e inmejorables para satisfacer las exigencias de nuestros clientes.<br/>&nbsp;</td>
		</tr>
</table>
</div>
{{< /rawhtml >}}


### Visión

{{< rawhtml >}}
<div style="text-align:center">
<table style="border:none;">
		<tr>
			<td style="border:none; text-align:justify">COLIMCOM S.A.S., será reconocida en las principales ciudades de Colombia por ser un aliado estratégico, así como una de las empresas más confiables y seguras en soluciones tecnológicas e industriales, apalancándose en una sólida estructura organizacional y tecnológica en continuo crecimiento con una gran presencia a nivel nacional enfocada en el servicio, calidad, profesionalismo e innovación.<br/>&nbsp;</td>
		</tr>
</table>
</div>
{{< /rawhtml >}}


### Valores Corporativos

{{< rawhtml >}}


<div style="text-align:center">
<table style="border:none;">
		<tr style="border:none; background-color:transparent;">
			<td style="border:none; background-color:transparent;" rowspan="3"><img src="https://www.colimcom.com/img/vineta.png"></td>
			<td style="border:none; background-color:transparent;"></td>
		</tr>
		<tr style="border:none; background-color:transparent;">
			<td style="border:none; background-color:transparent;">Cumplimiento</td>
		</tr>
		<tr style="border:none; background-color:transparent;">
			<td style="border:none; background-color:transparent;"></td>
		</tr>
		<tr style="border:none; background-color:transparent;">
			<td style="border:none; background-color:transparent;" rowspan="3"><img src="https://www.colimcom.com/img/vineta.png"></td>
			<td style="border:none; background-color:transparent;"></td>
		</tr>
		<tr style="border:none; background-color:transparent;">
			<td style="border:none; background-color:transparent;">Responsabilidad</td>
		</tr>
		<tr style="border:none; background-color:transparent;">
			<td style="border:none; background-color:transparent;"></td>
		</tr>
		<tr style="border:none; background-color:transparent;">
			<td style="border:none; background-color:transparent;" rowspan="3"><img src="https://www.colimcom.com/img/vineta.png"></td>
			<td style="border:none; background-color:transparent;"></td>
		</tr>
		<tr style="border:none; background-color:transparent;">
			<td style="border:none; background-color:transparent;">Calidad</td>
		</tr>
		<tr style="border:none; background-color:transparent;">
			<td style="border:none; background-color:transparent;"></td>
		</tr>
		<tr style="border:none; background-color:transparent;">
			<td style="border:none; background-color:transparent;" rowspan="3"><img src="https://www.colimcom.com/img/vineta.png"></td>
			<td style="border:none; background-color:transparent;"></td>
		</tr>
		<tr style="border:none; background-color:transparent;">
			<td style="border:none; background-color:transparent;">Confianza</td>
		</tr>
		<tr style="border:none; background-color:transparent;">
			<td style="border:none; background-color:transparent;"></td>
		</tr>
		<tr style="border:none; background-color:transparent;">
			<td style="border:none; background-color:transparent;" rowspan="3"><img src="https://www.colimcom.com/img/vineta.png"></td>
			<td style="border:none; background-color:transparent;"></td>
		</tr>
		<tr style="border:none; background-color:transparent;">
			<td style="border:none; background-color:transparent;">Respeto</td>
		</tr>
		<tr style="border:none; background-color:transparent;">
			<td style="border:none; background-color:transparent;"></td>
		</tr>

</table>
</div>
{{< /rawhtml >}}
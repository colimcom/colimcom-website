---
title: Gracias
subtitle: Muchas gracias por contactar con nosotros.
comments: false
---

Tu mensaje es muy importante para nosotros, por esa razón daremos respuesta a tu inquietud o solicitud lo antes posible.

{{< rawhtml >}}
<html>
<head>
<meta http-equiv = "refresh" content = "7; url = https://www.colimcom.com" />
</head>
</html>
{{< /rawhtml >}}
---
title: Importación
comments: false
---

### Nuestras importaciones

{{< rawhtml >}}
<div style="text-align:center">
<table style="border:none;">
		<tr>
			<td style="border:none; text-align:justify">Somos importadores de toda clase de mercancia, insumos y consumibles para equipos de oficina, de cómputo, equipos de informática, y en general todo lo que tenga que ver de forma directa o indirecta con dicho comercio.<br/>
&nbsp;<br/>
realizamos la importación  de suministros de papelería, aseo y cafetería, insumos y accesorios para computadores, impresoras, ferretería y muebles, servicios logísticos, así como todas las actividades de servicio relacionadas con la proveeduría integral para las oficinas, con venta directa, transaccional o a través de servicios de outsourcing, asesoría y transporte.<br/>&nbsp;</td>
		</tr>
</table>
</div>
<img src="https://www.colimcom.com/img/Importacion.jpg">
{{< /rawhtml >}}
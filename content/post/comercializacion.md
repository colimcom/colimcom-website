---
title: Comercialización
comments: false
---

### Comercio Empresarial

{{< rawhtml >}}
<div style="text-align:center">
<table style="border:none;">
		<tr>
			<td style="border:none; text-align:justify">Nos enfocamos en brindar a nuestros clientes las mejores soluciones en el mercado, ya sea en el ámbito técnico, científico o social, poniendo como meta principal la satisfacción total de nuestros clientes.<br/>&nbsp;</td>
		</tr>
</table>
</div>
<img src="https://www.colimcom.com/img/comercializacion.jpg">
{{< /rawhtml >}}